# Prueba técnica Planeta Huerto

## Instalación

Puede utilizarse el entorno de Homestead homestead-planetahuerto para levantar un entorno donde trabajar. Hay que cambiar el mapeo de directorio del proyecto para que se corresponda con el lugar de instalación.

Pasos a seguir:

1. Lanzar `vagrant up --provision`
2. Añadir al /etc/hosts `192.168.10.10 planetahuerto.test`
3. Entrar a la máquina con `vagrant ssh`
4. Ejecutar `composer install` desde la carpeta `/home/vagrant/planetahuerto`
    
## Uso

Pueden lanzarse los tests y consultar el coverage con:

`vendor/bin/phpunit --coverage-html public/coverage/`


## A tener en cuenta
* Se ha modelado siguiendo DDD y con Arquitectura Hexagonal. Como solo se ha realizado el modelado del dominio, no se han creado las carpetas de Application ni Infrastructure.

* La prueba se ha realizado aplicando TDD.

* Se ha utilizado el patrón ObjectMother en los tests para abstraer la llamada a los constructores.

* Aunque no se solicitaba expresamente, como se tiene una fecha de último abono para ver si se ha realizado en los últimos días, se ha considerado necesario crear una función para poder modificar esta fecha. La función es `abonar()` y genera un evento de dominio para notificar esta acción, solo que como no hay ningún bus de eventos no se envía a ningún broker.


## Mejoras

El proyecto podría mejorarse de diversas formas:

* Poner el código en inglés, ya que se ha hecho en castellano por desconocimiento del vocabulario. Siempre es mejor internacionarlo pensando en equipos de diferentes lugares.
* Añadir un validador de código como Codesniffer.
* Añadir un framework de Mutant Testing, como Infection.

## Cálculos en base a hemisferios

### Opción 1
Para calcular las fechas en base al hemisferio, una opción podría ser que los métodos que calculan reciban como parámetro el hemisferio.

Para evitar tener demasiada lógica dentro de Bonsai, podría tenerse un VO Hemisferio, que contenga el hemisferio que és y funciones para cálcular los meses de cada estación.

Por ejemplo:

````
public function necesitaAbono(Hemisferio $hemisferio): bool
{
    if (!$hemisferio->esPrimavera() || $hemisferio->esOtoño()) {
        return false;
    }

    if (!isset($this->ultimoAbono)
        || $this->ultimoAbono->value()->getTimestamp() < strtotime('-30 days')) {
        return true;
    }

    return false;
}

public function frecuenciaRiego(Hemisferio $hemisferio): string
{
    $month = date('n');
    if (in_array($month, $hemisferio->mesesMasCalurosos()) {
        return self::FRECUENCIA_RIEGO_VERANO;
    }

    return $this->frecuenciaNormalDeRiego();
}
````

La desventaja es que hay que pasar el hemisferio en cada llamada.

Se descarta que el Hemisferio forme parte de los atributos del Bonsai, porque conceptualmente hablando, un Bonsai no tiene un hemisferio.

### Opción 2

Otra posiblidad sería utilizar un servicio de dominio que se pase en cada llamada a las funciones, que haga todos los cálculos y sí que pueda recibir como parámetro del constructor el hemisferio, incluso podría pasársele por configuración a la hora de declararlo en el services.yaml. Por ejemplo, pasando una variable del entorno que dependa del dominio web en el que se está. Un dominio español sería hemisferio norte pero el dominio de Chile sería sur.

El problema de esta opción el mismo que el anterior, que hay que pasarlo en cada llamada.

Se descarta inyectarlo a través del constructor porque no encaja con la Arquitectura Hexagonal ni el principio de inversión de dependencias, ya que cuando se quisiera instanciar la entidad habría que pasarle el servicio, no puede hacerse un "autowire". Por lo que no es posible. Se tendría que pasar a través de cada método y es lo mismo que la opción anterior. 


