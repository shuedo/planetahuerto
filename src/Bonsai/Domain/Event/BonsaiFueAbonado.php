<?php

declare(strict_types=1);

namespace PlanetaHuerto\Bonsai\Domain\Event;

use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiId;
use PlanetaHuerto\Shared\Domain\Event\DomainEvent;
use PlanetaHuerto\Shared\Domain\ValueObject\Date;

final class BonsaiFueAbonado implements DomainEvent
{
    private BonsaiId $aggregateId;
    private Date $fechaAbono;

    /**
     * BonsaiFueAbonado constructor.
     * @param BonsaiId $aggregateId
     * @param Date $fechaAbono
     */
    public function __construct(BonsaiId $aggregateId, Date $fechaAbono)
    {
        $this->aggregateId = $aggregateId;
        $this->fechaAbono = $fechaAbono;
    }

    /**
     * @return BonsaiId
     */
    public function aggregateId(): BonsaiId
    {
        return $this->aggregateId;
    }

    /**
     * @return Date
     */
    public function fechaAbono(): Date
    {
        return $this->fechaAbono;
    }
}
