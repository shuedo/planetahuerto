<?php

declare(strict_types=1);

namespace PlanetaHuerto\Bonsai\Domain\ValueObject;

use PlanetaHuerto\Shared\Domain\ValueObject\StringValueObject;

final class BonsaiName extends StringValueObject
{

}