<?php

declare(strict_types=1);

namespace PlanetaHuerto\Bonsai\Domain\ValueObject;

use PlanetaHuerto\Shared\Domain\Aggregate\AggregateId;
use PlanetaHuerto\Shared\Domain\ValueObject\Uuid;

final class BonsaiId extends Uuid implements AggregateId
{

}