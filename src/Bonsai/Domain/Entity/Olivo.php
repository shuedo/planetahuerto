<?php

declare(strict_types=1);

namespace PlanetaHuerto\Bonsai\Domain\Entity;

final class Olivo extends Bonsai
{
    private const FRECUENCIA_RIEGO = "Poco frecuente";

    /**
     * @return string
     */
    protected function frecuenciaNormalDeRiego(): string
    {
        return self::FRECUENCIA_RIEGO;
    }
}