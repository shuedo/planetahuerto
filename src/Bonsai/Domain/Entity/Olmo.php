<?php

declare(strict_types=1);

namespace PlanetaHuerto\Bonsai\Domain\Entity;

final class Olmo extends Bonsai
{
    private const FRECUENCIA_RIEGO = "Muy frecuente";

    /**
     * @return string
     */
    protected function frecuenciaNormalDeRiego(): string
    {
        return self::FRECUENCIA_RIEGO;
    }

    /**
     * @return string
     */
    public function frecuenciaPulverizar(): string
    {
        return $this->frecuenciaRiego();
    }
}