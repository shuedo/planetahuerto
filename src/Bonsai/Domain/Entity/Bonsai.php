<?php

declare(strict_types=1);

namespace PlanetaHuerto\Bonsai\Domain\Entity;

use PlanetaHuerto\Bonsai\Domain\Event\BonsaiFueAbonado;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiId;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiName;
use PlanetaHuerto\Shared\Domain\Aggregate\AggregateRoot;
use PlanetaHuerto\Shared\Domain\ValueObject\Date;

abstract class Bonsai extends AggregateRoot
{
    private BonsaiId $id;
    private BonsaiName $name;
    private ?Date $ultimoAbono;

    private const FRECUENCIA_RIEGO_VERANO = "Muy frecuente";

    /**
     * Bonsai constructor.
     * @param BonsaiId $id
     * @param BonsaiName $name
     * @param Date|null $ultimoAbono
     */
    private function __construct(BonsaiId $id, BonsaiName $name, ?Date $ultimoAbono)
    {
        $this->id = $id;
        $this->name = $name;
        $this->ultimoAbono = $ultimoAbono;
    }

    /**
     * @param BonsaiId $id
     * @param BonsaiName $name
     * @param Date|null $ultimoAbono
     * @return static
     */
    public static function instantiate(BonsaiId $id, BonsaiName $name, ?Date $ultimoAbono): self
    {
        return new static($id, $name, $ultimoAbono);
    }

    /**
     * @return BonsaiId
     */
    public function id(): BonsaiId
    {
        return $this->id;
    }

    /**
     * @return BonsaiName
     */
    public function name(): BonsaiName
    {
        return $this->name;
    }

    /**
     * @return Date|null
     */
    public function ultimoAbono(): ?Date
    {
        return $this->ultimoAbono;
    }

    /**
     * @return void
     */
    public function abonar(): void
    {
        $this->ultimoAbono = Date::now();

        $this->recordThat(
            new BonsaiFueAbonado($this->id, $this->ultimoAbono)
        );
    }

    /**
     * @return bool
     */
    public function necesitaAbono(): bool
    {
        if (!$this->esOtonyoOPrimavera()) {
            return false;
        }

        return !isset($this->ultimoAbono)
            || $this->ultimoAbono->value()->getTimestamp() < strtotime('-30 days');
    }

    /**
     * @return bool
     */
    public function necesitaTransplantar(): bool
    {
        $month = date('n');
        if ($month == 3) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function frecuenciaRiego(): string
    {
        $month = date('n');
        if ($month == 7 || $month == 8) {
            return self::FRECUENCIA_RIEGO_VERANO;
        }

        return $this->frecuenciaNormalDeRiego();
    }

    /**
     * @return bool
     */
    private function esOtonyoOPrimavera(): bool
    {
        $day = date("z");

        $primaveraInicio = date("z", strtotime("March 21"));
        $primaveraFin = date("z", strtotime("June 20"));
        $otonyoInicio = date("z", strtotime("September 23"));
        $otonyoFin = date("z", strtotime("December 20"));

        if ($day >= $primaveraInicio && $day <= $primaveraFin) {
            return true;
        }

        if ($day >= $otonyoInicio && $day <= $otonyoFin) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    abstract protected function frecuenciaNormalDeRiego(): string;
}
