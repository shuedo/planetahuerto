<?php

declare(strict_types=1);

namespace PlanetaHuerto\Bonsai\Domain\Entity;

final class Ficus extends Bonsai
{
    private const FRECUENCIA_RIEGO = "Poco frecuente";

    protected function frecuenciaNormalDeRiego(): string
    {
        return self::FRECUENCIA_RIEGO;
    }
}