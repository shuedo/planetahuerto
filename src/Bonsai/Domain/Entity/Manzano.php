<?php

declare(strict_types=1);

namespace PlanetaHuerto\Bonsai\Domain\Entity;

final class Manzano extends Bonsai
{
    private const FRECUENCIA_RIEGO = "Frecuente";

    /**
     * @return string
     */
    protected function frecuenciaNormalDeRiego(): string
    {
        return self::FRECUENCIA_RIEGO;
    }
}