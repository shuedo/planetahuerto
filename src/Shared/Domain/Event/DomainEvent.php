<?php

declare(strict_types=1);

namespace PlanetaHuerto\Shared\Domain\Event;

use PlanetaHuerto\Shared\Domain\Aggregate\AggregateId;

interface DomainEvent
{
    /**
     * @return AggregateId
     */
    public function aggregateId(): AggregateId;
}