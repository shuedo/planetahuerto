<?php

declare(strict_types=1);

namespace PlanetaHuerto\Shared\Domain\ValueObject;

use Exception;
use InvalidArgumentException;
use Ramsey\Uuid\Uuid as RamseyUuid;

class Uuid
{
    protected string $value;

    /**
     * Uuid constructor.
     * @param string $value
     */
    protected function __construct(string $value)
    {
        $this->ensureIsValidUuid($value);

        $this->value = $value;
    }

    /**
     * @param string $aggregateId
     * @return static
     */
    public static function fromString(string $aggregateId): self
    {
        return new static($aggregateId);
    }

    /**
     * @return static
     * @throws Exception
     */
    public static function random(): self
    {
        return new static(RamseyUuid::uuid4()->toString());
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @param string $id
     */
    private function ensureIsValidUuid(string $id): void
    {
        if (!RamseyUuid::isValid($id)) {
            throw new InvalidArgumentException(sprintf('<%s> does not allow the value <%s>.', static::class, $id), 400);
        }
    }
}