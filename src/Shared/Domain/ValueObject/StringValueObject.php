<?php

declare(strict_types=1);

namespace PlanetaHuerto\Shared\Domain\ValueObject;

abstract class StringValueObject
{
    protected string $value;

    /**
     * StringValueObject constructor.
     * @param string $value
     */
    protected function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @param string|null $string
     * @return static
     */
    public static function fromString(string $string): self
    {
        return new static($string);
    }
}
