<?php

declare(strict_types=1);

namespace PlanetaHuerto\Shared\Domain\ValueObject;

use DateTime;
use DateTimeZone;
use Exception;

abstract class DateTimeValueObject
{
    private const DEFAULT_FORMAT = 'Y-m-d H:i:s';
    private const DEFAULT_TIMEZONE = 'Europe/Madrid';

    /**
     * @var DateTime
     */
    protected DateTime $value;

    /**
     * @param DateTime $value
     */
    protected function __construct(DateTime $value)
    {
        $this->value = $value;
    }

    /**
     * @return static
     */
    public static function now(): self
    {
        return new static(new DateTime());
    }

    /**
     * @param string|null $date
     * @param string $timezone
     * @return static
     * @throws Exception
     */
    public static function fromString(string $date, string $timezone = self::DEFAULT_TIMEZONE): self
    {
        $dateTime = new DateTime($date, new DateTimeZone($timezone));

        return new static($dateTime);
    }

    /**
     * @return DateTime
     */
    public function value(): DateTime
    {
        return $this->value;
    }

    /**
     * @param string $format
     * @return string
     */
    public function format(string $format = self::DEFAULT_FORMAT): string
    {
        return $this->value()->format($format);
    }
}