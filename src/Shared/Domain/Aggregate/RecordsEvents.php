<?php

declare(strict_types=1);

namespace PlanetaHuerto\Shared\Domain\Aggregate;

use PlanetaHuerto\Shared\Domain\Event\DomainEvents;

interface RecordsEvents
{
    /**
     * @return DomainEvents
     */
    public function recordedEvents(): DomainEvents;
}