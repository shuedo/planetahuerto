<?php

declare(strict_types=1);

namespace PlanetaHuerto\Shared\Domain\Aggregate;

use PlanetaHuerto\Shared\Domain\Event\DomainEvent;
use PlanetaHuerto\Shared\Domain\Event\DomainEvents;

abstract class AggregateRoot implements RecordsEvents
{
    /**
     * @var array|DomainEvent[]
     */
    private array $recordedEvents = [];

    /**
     * @return DomainEvents
     */
    public function recordedEvents(): DomainEvents
    {
        return new DomainEvents($this->recordedEvents);
    }

    /**
     * @param DomainEvent $event
     * @return void
     */
    protected function recordThat(DomainEvent $event): void
    {
        $this->recordedEvents[] = $event;
    }
}