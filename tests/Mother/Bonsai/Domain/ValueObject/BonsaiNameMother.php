<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject;

use Faker\Factory;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiName;

final class BonsaiNameMother
{
    /**
     * @param string $bonsaiName
     * @return BonsaiName
     */
    public static function create(string $bonsaiName): BonsaiName
    {
        return BonsaiName::fromString($bonsaiName);
    }

    /**
     * @return BonsaiName
     */
    public static function random(): BonsaiName
    {
        $faker = Factory::create();
        return self::create($faker->word);
    }
}