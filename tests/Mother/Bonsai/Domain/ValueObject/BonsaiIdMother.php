<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject;

use Exception;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiId;

final class BonsaiIdMother
{
    /**
     * @param string $bonsaiId
     * @return BonsaiId
     */
    public static function create(string $bonsaiId): BonsaiId
    {
        return BonsaiId::fromString($bonsaiId);
    }

    /**
     * @return BonsaiId
     * @throws Exception
     */
    public static function random(): BonsaiId
    {
        return self::create(BonsaiId::random()->value());
    }
}