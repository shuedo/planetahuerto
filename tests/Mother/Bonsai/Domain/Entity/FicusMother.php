<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Mother\Bonsai\Domain\Entity;

use Exception;
use PlanetaHuerto\Bonsai\Domain\Entity\Ficus;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiId;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiName;
use PlanetaHuerto\Shared\Domain\ValueObject\Date;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject\BonsaiIdMother;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject\BonsaiNameMother;
use PlanetaHuerto\Tests\Mother\Shared\Domain\ValueObject\DateMother;

final class FicusMother
{
    /**
     * @param BonsaiId $id
     * @param BonsaiName $name
     * @param Date|null $ultimoAbono
     * @return Ficus
     */
    public static function create(BonsaiId $id, BonsaiName $name, ?Date $ultimoAbono): Ficus
    {
        return Ficus::instantiate($id, $name, $ultimoAbono);
    }

    /**
     * @return Ficus
     * @throws Exception
     */
    public static function random(): Ficus
    {
        return self::create(
            BonsaiIdMother::random(),
            BonsaiNameMother::random(),
            DateMother::random()
        );
    }
}