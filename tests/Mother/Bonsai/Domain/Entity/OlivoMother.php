<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Mother\Bonsai\Domain\Entity;

use Exception;
use PlanetaHuerto\Bonsai\Domain\Entity\Olivo;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiId;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiName;
use PlanetaHuerto\Shared\Domain\ValueObject\Date;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject\BonsaiIdMother;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject\BonsaiNameMother;
use PlanetaHuerto\Tests\Mother\Shared\Domain\ValueObject\DateMother;

final class OlivoMother
{
    /**
     * @param BonsaiId $id
     * @param BonsaiName $typeName
     * @param Date|null $ultimoAbono
     * @return Olivo
     */
    public static function create(BonsaiId $id, BonsaiName $typeName, ?Date $ultimoAbono): Olivo
    {
        return Olivo::instantiate($id, $typeName, $ultimoAbono);
    }

    /**
     * @return Olivo
     * @throws Exception
     */
    public static function random(): Olivo
    {
        return self::create(
            BonsaiIdMother::random(),
            BonsaiNameMother::random(),
            DateMother::random()
        );
    }
}