<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Mother\Bonsai\Domain\Entity;

use Exception;
use PlanetaHuerto\Bonsai\Domain\Entity\Manzano;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiId;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiName;
use PlanetaHuerto\Shared\Domain\ValueObject\Date;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject\BonsaiIdMother;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject\BonsaiNameMother;
use PlanetaHuerto\Tests\Mother\Shared\Domain\ValueObject\DateMother;

final class ManzanoMother
{
    /**
     * @param BonsaiId $id
     * @param BonsaiName $typeName
     * @param Date|null $ultimoAbono
     * @return Manzano
     */
    public static function create(BonsaiId $id, BonsaiName $typeName, ?Date $ultimoAbono): Manzano
    {
        return Manzano::instantiate($id, $typeName, $ultimoAbono);
    }

    /**
     * @return Manzano
     * @throws Exception
     */
    public static function random(): Manzano
    {
        return self::withUltimoAbonoDate(DateMother::random()->format());
    }

    /**
     * @param string $date
     * @return Manzano
     * @throws Exception
     */
    public static function withUltimoAbonoDate(string $date): Manzano
    {
        return self::create(
            BonsaiIdMother::random(),
            BonsaiNameMother::random(),
            DateMother::create($date)
        );
    }
}