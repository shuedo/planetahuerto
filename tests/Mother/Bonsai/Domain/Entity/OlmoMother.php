<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Mother\Bonsai\Domain\Entity;

use Exception;
use PlanetaHuerto\Bonsai\Domain\Entity\Olmo;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiId;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiName;
use PlanetaHuerto\Shared\Domain\ValueObject\Date;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject\BonsaiIdMother;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject\BonsaiNameMother;
use PlanetaHuerto\Tests\Mother\Shared\Domain\ValueObject\DateMother;

final class OlmoMother
{
    /**
     * @param BonsaiId $id
     * @param BonsaiName $typeName
     * @param Date|null $ultimoAbono
     * @return Olmo
     */
    public static function create(BonsaiId $id, BonsaiName $typeName, ?Date $ultimoAbono): Olmo
    {
        return Olmo::instantiate($id, $typeName, $ultimoAbono);
    }

    /**
     * @return Olmo
     * @throws Exception
     */
    public static function random(): Olmo
    {
        return self::create(
            BonsaiIdMother::random(),
            BonsaiNameMother::random(),
            DateMother::random()
        );
    }
}