<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Mother\Shared\Domain\ValueObject;

use Exception;
use Faker\Factory;
use PlanetaHuerto\Shared\Domain\ValueObject\Date;

final class DateMother
{
    /**
     * @param string $date
     * @return Date
     * @throws Exception
     */
    public static function create(string $date): Date
    {
        return Date ::fromString($date);
    }

    /**
     * @return Date
     * @throws Exception
     */
    public static function random(): Date
    {
        $faker = Factory::create();
        return self::create($faker->date());
    }
}