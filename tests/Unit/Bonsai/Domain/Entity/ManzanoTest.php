<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Unit\Bonsai\Domain\Entity;

use Exception;
use PHPUnit\Framework\TestCase;
use PlanetaHuerto\Bonsai\Domain\Entity\Manzano;
use PlanetaHuerto\Bonsai\Domain\Event\BonsaiFueAbonado;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiId;
use PlanetaHuerto\Bonsai\Domain\ValueObject\BonsaiName;
use PlanetaHuerto\Shared\Domain\ValueObject\Date;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\Entity\ManzanoMother;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject\BonsaiIdMother;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\ValueObject\BonsaiNameMother;
use PlanetaHuerto\Tests\Mother\Shared\Domain\ValueObject\DateMother;
use Symfony\Bridge\PhpUnit\ClockMock;

class ManzanoTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testInstantiate()
    {
        $id = BonsaiIdMother::random();
        $name = BonsaiNameMother::random();
        $ultimoAbono = DateMother::random();

        $manzano = Manzano::instantiate($id, $name, $ultimoAbono);

        $this->assertEquals($id->value(), $manzano->id()->value());
        $this->assertEquals($name->value(), $manzano->name()->value());
        $this->assertEquals($ultimoAbono->format(), $manzano->ultimoAbono()->format());
    }

    /**
     * @throws Exception
     */
    public function testAbonar()
    {

        $manzano = ManzanoMother::random();

        $manzano->abonar();
        $this->assertEquals(Date::now()->format('Y-m-d'), $manzano->ultimoAbono()->format('Y-m-d'));

        /** @var BonsaiFueAbonado $ultimoEvento */
        $ultimoEvento = $manzano->recordedEvents()->getIterator()->offsetGet(0);
        $this->assertInstanceOf(BonsaiFueAbonado::class, $ultimoEvento);
        $this->assertEquals($ultimoEvento->aggregateId()->value(), $manzano->id()->value());
        $this->assertEquals($ultimoEvento->fechaAbono()->format(), $manzano->ultimoAbono()->format());
    }

    /**
     * @throws Exception
     */
    public function testTengoQueAbonarEnPrimavera()
    {

        ClockMock::register(Manzano::class);
        ClockMock::withClockMock(date_create('2021-04-01')->getTimestamp());

        $manzano = ManzanoMother::withUltimoAbonoDate('2020-01-01');

        $this->assertTrue($manzano->necesitaAbono());
    }

    /**
     * @throws Exception
     */
    public function testTengoQueAbonarEnOtonyo()
    {

        ClockMock::register(Manzano::class);
        ClockMock::withClockMock(date_create('2021-10-01')->getTimestamp());

        $manzano = ManzanoMother::withUltimoAbonoDate('2020-01-01');

        $this->assertTrue($manzano->necesitaAbono());
    }

    /**
     * @throws Exception
     */
    public function testTengoQueAbonarEnInvierno()
    {

        ClockMock::register(Manzano::class);
        ClockMock::withClockMock(date_create('2021-01-01')->getTimestamp());

        $manzano = ManzanoMother::withUltimoAbonoDate('2020-01-01');

        $this->assertFalse($manzano->necesitaAbono());
    }

    /**
     * @throws Exception
     */
    public function testNuncaAbonado()
    {
        ClockMock::register(Manzano::class);
        ClockMock::withClockMock(date_create('2021-04-30')->getTimestamp());

        $manzano = ManzanoMother::create(
            BonsaiIdMother::random(),
            BonsaiNameMother::random(),
            null
        );

        $this->assertTrue($manzano->necesitaAbono());
    }

    /**
     * @throws Exception
     */
    public function testAbonadoEnlosUltimos30Dias()
    {
        ClockMock::register(Manzano::class);
        ClockMock::withClockMock(date_create('2021-04-30')->getTimestamp());

        $manzano = ManzanoMother::withUltimoAbonoDate(Date::now()->format());

        $this->assertFalse($manzano->necesitaAbono());
    }

    /**
     * @throws Exception
     */
    public function testTransplantar()
    {
        ClockMock::register(Manzano::class);
        ClockMock::withClockMock(date_create('2021-03-10')->getTimestamp());

        $manzano = ManzanoMother::random();

        $this->assertTrue($manzano->necesitaTransplantar());
    }

    /**
     * @throws Exception
     */
    public function testNoTransplantar()
    {
        ClockMock::register(Manzano::class);
        ClockMock::withClockMock(date_create('2021-01-10')->getTimestamp());

        $manzano = ManzanoMother::random();

        $this->assertFalse($manzano->necesitaTransplantar());
    }

    /**
     * @throws Exception
     */
    public function testRiegoEnJulio()
    {
        ClockMock::register(Manzano::class);
        ClockMock::withClockMock(date_create('2021-07-10')->getTimestamp());

        $manzano = ManzanoMother::random();

        $this->assertEquals("Muy frecuente", $manzano->frecuenciaRiego());
    }

    /**
     * @throws Exception
     */
    public function testRiegoEnAgosto()
    {
        ClockMock::register(Manzano::class);
        ClockMock::withClockMock(date_create('2021-08-10')->getTimestamp());

        $manzano = ManzanoMother::random();

        $this->assertEquals("Muy frecuente", $manzano->frecuenciaRiego());
    }

    /**
     * @throws Exception
     */
    public function testRiegoEnMesNormal()
    {
        ClockMock::register(Manzano::class);
        ClockMock::withClockMock(date_create('2021-02-10')->getTimestamp());

        $manzano = ManzanoMother::random();

        $this->assertEquals("Frecuente", $manzano->frecuenciaRiego());
    }
}