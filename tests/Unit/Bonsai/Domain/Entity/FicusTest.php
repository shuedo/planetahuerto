<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Unit\Bonsai\Domain\Entity;

use Exception;
use PHPUnit\Framework\TestCase;
use PlanetaHuerto\Bonsai\Domain\Entity\Ficus;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\Entity\FicusMother;
use Symfony\Bridge\PhpUnit\ClockMock;

class FicusTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testRiegoEnJulio()
    {
        ClockMock::register(Ficus::class);
        ClockMock::withClockMock(date_create('2021-07-10')->getTimestamp());

        $ficus = FicusMother::random();

        $this->assertEquals("Muy frecuente", $ficus->frecuenciaRiego());
    }

    /**
     * @throws Exception
     */
    public function testRiegoEnAgosto()
    {
        ClockMock::register(Ficus::class);
        ClockMock::withClockMock(date_create('2021-08-10')->getTimestamp());

        $ficus = FicusMother::random();

        $this->assertEquals("Muy frecuente", $ficus->frecuenciaRiego());
    }

    /**
     * @throws Exception
     */
    public function testRiegoEnMesNormal()
    {
        ClockMock::register(Ficus::class);
        ClockMock::withClockMock(date_create('2021-02-10')->getTimestamp());

        $ficus = FicusMother::random();

        $this->assertEquals("Poco frecuente", $ficus->frecuenciaRiego());
    }
}