<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Unit\Bonsai\Domain\Entity;

use Exception;
use PHPUnit\Framework\TestCase;
use PlanetaHuerto\Bonsai\Domain\Entity\Olivo;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\Entity\OlivoMother;
use Symfony\Bridge\PhpUnit\ClockMock;

class OlivoTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testRiegoEnJulio() {
        ClockMock::register(Olivo::class);
        ClockMock::withClockMock(date_create('2021-07-10')->getTimestamp());

        $olivo = OlivoMother::random();

        $this->assertEquals("Muy frecuente", $olivo->frecuenciaRiego());
    }

    /**
     * @throws Exception
     */
    public function testRiegoEnAgosto() {
        ClockMock::register(Olivo::class);
        ClockMock::withClockMock(date_create('2021-08-10')->getTimestamp());

        $olivo = OlivoMother::random();

        $this->assertEquals("Muy frecuente", $olivo->frecuenciaRiego());
    }

    /**
     * @throws Exception
     */
    public function testRiegoEnMesNormal() {
        ClockMock::register(Olivo::class);
        ClockMock::withClockMock(date_create('2021-02-10')->getTimestamp());

        $olivo = OlivoMother::random();

        $this->assertEquals("Poco frecuente", $olivo->frecuenciaRiego());
    }
}