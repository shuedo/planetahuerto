<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Unit\Bonsai\Domain\Entity;

use Exception;
use PHPUnit\Framework\TestCase;
use PlanetaHuerto\Bonsai\Domain\Entity\Olmo;
use PlanetaHuerto\Tests\Mother\Bonsai\Domain\Entity\OlmoMother;
use Symfony\Bridge\PhpUnit\ClockMock;

class OlmoTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testRiegoEnJulio()
    {
        ClockMock::register(Olmo::class);
        ClockMock::withClockMock(date_create('2021-07-10')->getTimestamp());

        $olmo = OlmoMother::random();

        $this->assertEquals("Muy frecuente", $olmo->frecuenciaRiego());
    }

    /**
     * @throws Exception
     */
    public function testRiegoEnAgosto()
    {
        ClockMock::register(Olmo::class);
        ClockMock::withClockMock(date_create('2021-08-10')->getTimestamp());

        $olmo = OlmoMother::random();

        $this->assertEquals("Muy frecuente", $olmo->frecuenciaRiego());
    }

    /**
     * @throws Exception
     */
    public function testRiegoEnMesNormal()
    {
        ClockMock::register(Olmo::class);
        ClockMock::withClockMock(date_create('2021-02-10')->getTimestamp());

        $olmo = OlmoMother::random();

        $this->assertEquals("Muy frecuente", $olmo->frecuenciaRiego());
    }

    /**
     * @throws Exception
     */
    public function testPulverizarEnJulio()
    {
        ClockMock::register(Olmo::class);
        ClockMock::withClockMock(date_create('2021-07-10')->getTimestamp());

        $olmo = OlmoMother::random();

        $this->assertEquals("Muy frecuente", $olmo->frecuenciaPulverizar());
    }

    /**
     * @throws Exception
     */
    public function testPulverizarEnAgosto()
    {
        ClockMock::register(Olmo::class);
        ClockMock::withClockMock(date_create('2021-08-10')->getTimestamp());

        $olmo = OlmoMother::random();

        $this->assertEquals("Muy frecuente", $olmo->frecuenciaPulverizar());
    }

    /**
     * @throws Exception
     */
    public function testPulverizarEnMesNormal()
    {
        ClockMock::register(Olmo::class);
        ClockMock::withClockMock(date_create('2021-02-10')->getTimestamp());

        $olmo = OlmoMother::random();

        $this->assertEquals("Muy frecuente", $olmo->frecuenciaPulverizar());
    }
}