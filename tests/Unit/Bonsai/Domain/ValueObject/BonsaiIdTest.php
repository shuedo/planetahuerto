<?php

declare(strict_types=1);

namespace PlanetaHuerto\Tests\Unit\Bonsai\Domain\ValueObject;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use PlanetaHuerto\Shared\Domain\ValueObject\Uuid;

class BonsaiIdTest extends TestCase
{
    public function testValidUuid()
    {

        $bonsaiId = Uuid::fromString("f8f910c0-ee60-478b-9c55-c424bc37b0f9");
        $this->assertEquals("f8f910c0-ee60-478b-9c55-c424bc37b0f9", $bonsaiId->value());
    }

    public function testInvalid()
    {

        $this->expectException(InvalidArgumentException::class);
        Uuid::fromString("wrong");
    }
}